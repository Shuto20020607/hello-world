import java.util.Scanner;
public class HelloWorld {
    static void order(String food){
        System.out.println("What would you like to order:");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");
    }
    public static void main(String[] args) {
        /*System.out.println("What would you like to order:");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");*/
        Scanner userIn = new Scanner(System.in);
        System.out.print("Your order:");
        int number=userIn.nextInt();
        userIn.close();
        if(number==1) {
            System.out.println("We have received your order for Tempura. It will be served soon!");
        }
        if(number==2) {
            System.out.println("We have received your order for Ramen. It will be served soon!");
        }
        if(number==3) {
            System.out.println("We have received your order for Udon. It will be served soon!");
        }
    }
}
